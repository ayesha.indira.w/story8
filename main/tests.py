# Create your tests here.
from django.test import TestCase, Client
from .views import index
from django.urls import resolve
from django.http import HttpRequest
from django.apps import apps

# Create your tests here.
class TestStory8(TestCase):
    def test_apakah_url_story8_ada(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)
    def test_apakah_story7_ada_templatenya(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    def test_apakah_ada_text_story_8_di_halamannya(self):
        response = Client().get('/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Story 8", html_kembalian)
    def test_url_json(self):
        response = Client().get("/data/")
        self.assertEqual(response.status_code, 200)

    